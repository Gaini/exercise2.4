var app = require('express')();
var http = require('http').Server(app);
var io = require('socket.io')(http);
const bodyParser = require("body-parser");
var port = 3000;

app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.json());

app.get('/', function(req, res){
    res.sendFile(__dirname + '/index.html');
});

io.on('connection', function(socket){

    socket.on('chat message', function(msg){
        io.emit('chat message', msg);
    });

    socket.on('add user', function (username,room) {
        socket.broadcast.emit('chat message', 'Новый пользователь - '+username+' в комнате - '+room);
    });

});


http.listen(port, function(){
    console.log('listening on *:' + port);
});